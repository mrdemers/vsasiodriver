/* This is the header file for VisiSonics audio subsystem interface DLL */

#define	_VS_AIF_NO_ERROR			0
                                                        
#define	_VS_AIF_BASE_ERROR_NUMBER		-16383

#define	_VS_AIF_INTERNAL_ERROR			(_VS_AIF_BASE_ERROR_NUMBER -  1)
#define	_VS_AIF_INIT_REQUIRED			(_VS_AIF_BASE_ERROR_NUMBER -  2)
#define	_VS_AIF_INIT_ALREADY_DONE		(_VS_AIF_BASE_ERROR_NUMBER -  3)
#define	_VS_AIF_THREAD_LAUNCH_FAILURE		(_VS_AIF_BASE_ERROR_NUMBER -  4)
#define	_VS_AIF_MEMORY_ALLOCATION_FAILURE	(_VS_AIF_BASE_ERROR_NUMBER -  5)
#define	_VS_AIF_UNSUPPORTED_CONFIGURATION	(_VS_AIF_BASE_ERROR_NUMBER -  6)
#define	_VS_AIF_NO_HANDLES_AVAILABLE		(_VS_AIF_BASE_ERROR_NUMBER -  7)
#define	_VS_AIF_COMMUNICATOR_CREATE_FAILURE	(_VS_AIF_BASE_ERROR_NUMBER -  8)
#define	_VS_AIF_NONINTEGER_BLOCK_COUNT		(_VS_AIF_BASE_ERROR_NUMBER -  9)
#define	_VS_AIF_BUFFER_SIZE_TOO_SMALL		(_VS_AIF_BASE_ERROR_NUMBER - 10)
#define	_VS_AIF_DEVICE_OPEN_FAILURE		(_VS_AIF_BASE_ERROR_NUMBER - 11)
#define	_VS_AIF_DCLOCK_PROGRAMMING_FAILURE	(_VS_AIF_BASE_ERROR_NUMBER - 12)
#define	_VS_AIF_DEVICE_PROGRAMMING_FAILURE	(_VS_AIF_BASE_ERROR_NUMBER - 13)
#define	_VS_AIF_OUT_OF_RANGE_HANDLE		(_VS_AIF_BASE_ERROR_NUMBER - 14)
#define	_VS_AIF_INVALID_HANDLE			(_VS_AIF_BASE_ERROR_NUMBER - 15)
#define	_VS_AIF_DATA_NOT_READY			(_VS_AIF_BASE_ERROR_NUMBER - 16)
#define	_VS_AIF_HANDLE_NOT_RUNNING		(_VS_AIF_BASE_ERROR_NUMBER - 17)
#define	_VS_AIF_HANDLE_IS_RUNNING		(_VS_AIF_BASE_ERROR_NUMBER - 18)
#define	_VS_AIF_IQUERY_CONSTRUCTOR_FAILURE	(_VS_AIF_BASE_ERROR_NUMBER - 19)
#define	_VS_AIF_IQUERY_DEVICE_COUNT_FAILURE	(_VS_AIF_BASE_ERROR_NUMBER - 20)
#define	_VS_AIF_IQUERY_ZERO_DEVICE_COUNT	(_VS_AIF_BASE_ERROR_NUMBER - 21)
#define	_VS_AIF_IQUERY_DEVICE_COUNT_TOO_LARGE	(_VS_AIF_BASE_ERROR_NUMBER - 22)
#define	_VS_AIF_DEVICE_INDEX_INVALID		(_VS_AIF_BASE_ERROR_NUMBER - 23)
#define	_VS_AIF_DEVICE_INDEX_OUT_OF_RANGE	(_VS_AIF_BASE_ERROR_NUMBER - 24)

#ifdef _DLL_BUILD_PROCESS_
#define AIF_API __declspec(dllexport)
#else
#define AIF_API
#endif
// -------------------------------------------------------------
// _vs_aif_get_version -- retrieve library version
// Input:  None
// Output: major / minor versions and build date (YYYYMMDD)
// Return: Error code
// -------------------------------------------------------------
AIF_API
int _vs_aif_get_version(int *_major_version,
	int *_minor_version, int *_build_date);
// -------------------------------------------------------------


// -------------------------------------------------------------
// _vs_aif_init -- initialize the library
// Input:  None
// Output: None
// Return: Error code
// -------------------------------------------------------------
AIF_API
int _vs_aif_init(void);
// -------------------------------------------------------------


// -------------------------------------------------------------
// _vs_aif_get_device_count -- get device count
// Input:  None
// Output: A count of how many acquisition boards are connected
// Return: Error code
// -------------------------------------------------------------
AIF_API
int _vs_aif_get_device_count(int *_device_count);
// -------------------------------------------------------------


// -------------------------------------------------------------
// _vs_aif_get_device_serial -- get device serial number
// Input:  Board index, from zero to (_device_count - 1)
// Output: 10-byte long board serial number, NOT null-terminated
// Return: Error code
// -------------------------------------------------------------
AIF_API
int _vs_aif_get_device_serial(int _device_index, char *_serial);
// -------------------------------------------------------------


// -------------------------------------------------------------
// _vs_aif_acquisition_open -- open the acquisition device
// Input:  _device_index:  board index, from 0 to (count - 1)
//         _channel_count: number of channels
//                         (currently only 64 supported)
//         _block_size:    number of samples (per channel)
//                         in one data transfer block
//         _buffer_size:   size of background data buffer
//         _cfg_fname:     device configuration file name
//         _reserved:      reserved, must be zero
// Output: _handle:        a handle to the open device
// Return: Error code
// -------------------------------------------------------------
AIF_API
int _vs_aif_acquisition_open(int *_handle,
	int _device_index, int _channel_count, int _block_size,
	int _buffer_size, char *_cfg_fname, int _reserved);
// -------------------------------------------------------------


// -------------------------------------------------------------
// _vs_aif_acquisition_start -- start the acquisition process
// Input:  _handle: a handle to open device
//                  (obtained from _vs_acquisition_open)
// Output: None
// Return: Error code
// -------------------------------------------------------------
AIF_API
int _vs_aif_acquisition_start(int _handle);
// -------------------------------------------------------------


// -------------------------------------------------------------
// _vs_aif_retrieve_data_block -- obtain acquired data
// Input:  _handle: a handle to open device
//                  (obtained from _vs_acquisition_open)
//         _data:   a pointer to the data buffer;
//                  the buffer should be allocated by caller;
//                  the buffer size should be at least
//                  _channel_count * _block_size * sizeof(int)
// Output: _data:   buffer filled with deinterleaved samples;
//                  i.e. _block_size samples of channel 0,
//                  then _block_size samples of channel 1,
//                  and so on until channel 63. Transfers
//                  are always done in _block_size units.
// Return: Error code
// -------------------------------------------------------------
// Note:   This function is non-blocking. If you call it and
//         there is not enough data yet to fully populate the
//         buffer, the function returns _VS_AIF_DATA_NOT_READY
//         error code. This is not a fatal error; it simply
//         means "try again later".
// -------------------------------------------------------------
AIF_API
int _vs_aif_retrieve_data_block(int _handle, int *_data);
// -------------------------------------------------------------


// -------------------------------------------------------------
// _vs_aif_acquisition_stop -- terminate the acquisition process
// Input:  _handle: a handle to open device
//                  (obtained from _vs_acquisition_open)
// Output: None
// Return: Error code
// -------------------------------------------------------------
AIF_API
int _vs_aif_acquisition_stop(int _handle);
// -------------------------------------------------------------


// -------------------------------------------------------------
// _vs_aif_acquisition_close -- close the acquisition device
// Input:  _handle: a handle to open device
//                  (obtained from _vs_acquisition_open)
// Output: None
// Return: Error code
// -------------------------------------------------------------
AIF_API
int _vs_aif_acquisition_close(int _handle);
// -------------------------------------------------------------
