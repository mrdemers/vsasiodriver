#pragma once

#include <string>
#include <vector>
#include <memory>
enum
{
	cameraBufferSize = 1024,
	maxNumInputs = 256,
	numOutputs = 2
};

class CameraController
{
public:
	static void createInstance(std::string iniFilePath);
	static CameraController& getInstance();
	CameraController(std::string iniFilePath);
	~CameraController();

	int getDataBlock(int* inputBuffer);

	double getSampleRate();
	std::vector<int>& getChannelMap();
	int getNumInputChannels();

	void start();
	void stop();
private:
	CameraController() {}
	CameraController(CameraController& other) {}

	enum GainSetting
	{
		Low,
		Medium,
		High
	};

	std::vector<int> cameraChannelMap;
	double sampleRate;
	int aifHandle;

	static std::unique_ptr<CameraController> cameraController;
};