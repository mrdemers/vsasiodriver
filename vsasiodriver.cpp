/*
	Visisonics ASIO Driver
	Author: Matt Demers

	** Based on the asiosmpl from the asio sdk
*/

#include "vsasiodriver.h"

#include <stdio.h>
#include <string.h>
#include <sstream>
#include <iostream>
#include <chrono>

//------------------------------------------------------------------------------------------

// extern
void getNanoSeconds(ASIOTimeStamp *time);

// local

double AsioSamples2double (ASIOSamples* samples);

static const double twoRaisedTo32 = 4294967296.;
static const double twoRaisedTo32Reciprocal = 1. / twoRaisedTo32;

//------------------------------------------------------------------------------------------
// on windows, we do the COM stuff.

#if WINDOWS
#include "windows.h"
#include "mmsystem.h"

// class id.
// {9093E804-E7AA-484B-BD63-7805E272B0F1}
CLSID IID_ASIO_DRIVER = { 0x9093e804, 0xe7aa, 0x484b, { 0xbd, 0x63, 0x78, 0x05, 0xe2, 0x72, 0xb0, 0xf1 } };


CFactoryTemplate g_Templates[1] = {
    {L"VSASIODRIVER", &IID_ASIO_DRIVER, VSAsioDriver::CreateInstance} 
};
int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);

CUnknown* VSAsioDriver::CreateInstance (LPUNKNOWN pUnk, HRESULT *phr)
{
	return (CUnknown*)new VSAsioDriver (pUnk,phr);
};

STDMETHODIMP VSAsioDriver::NonDelegatingQueryInterface (REFIID riid, void ** ppv)
{
	if (riid == IID_ASIO_DRIVER)
	{
		return GetInterface (this, ppv);
	}
	return CUnknown::NonDelegatingQueryInterface (riid, ppv);
}

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//		Register ASIO Driver
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
extern LONG RegisterAsioDriver (CLSID,char *,char *,char *,char *);
extern LONG UnregisterAsioDriver (CLSID,char *,char *);

//
// Server registration, called on REGSVR32.EXE "the dllname.dll"
//
HRESULT _stdcall DllRegisterServer()
{
	LONG	rc;
	char	errstr[128];

	rc = RegisterAsioDriver (IID_ASIO_DRIVER,"vsAsioDriver.dll","VS ASIO Driver","Visisonics ASIO Dirver","Apartment");

	if (rc) {
		memset(errstr,0,128);
		sprintf(errstr,"Register Server failed ! (%d)",rc);
		MessageBox(0,(LPCTSTR)errstr,(LPCTSTR)"VS ASIO Driver",MB_OK);
		return -1;
	}

	return S_OK;
}

//
// Server unregistration
//
HRESULT _stdcall DllUnregisterServer()
{
	LONG	rc;
	char	errstr[128];

	rc = UnregisterAsioDriver (IID_ASIO_DRIVER,"vsAsioDriver.dll","VS ASIO Driver");

	if (rc) {
		memset(errstr,0,128);
		sprintf(errstr,"Unregister Server failed ! (%d)",rc);
		MessageBox(0,(LPCTSTR)errstr,(LPCTSTR)"VS ASIO I/O Driver",MB_OK);
		return -1;
	}

	return S_OK;
}

std::string getDllFilePath()
{
	char path[1024] = { 0 };
	HMODULE hm = nullptr;

	if (!GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
		(LPCSTR)&DllUnregisterServer, &hm))
	{
		return "";
	}
	GetModuleFileNameA(hm, path, sizeof(path));
	return std::string(path);
}

//------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------
VSAsioDriver::VSAsioDriver (LPUNKNOWN pUnk, HRESULT *phr)
	: CUnknown("ASIOSAMPLE", pUnk, phr)

//------------------------------------------------------------------------------------------

#else

// when not on windows, we derive from AsioDriver
VSAsioDriver::VSAsioDriver() : AsioDriver ()

#endif
{
	auto dllPath = getDllFilePath();
	auto lastSlashPosition = dllPath.rfind("\\");
	auto dllFolder = dllPath.substr(0, lastSlashPosition+1);
	auto iniFilePath = dllFolder + "vsAsioDriver.ini";
	CameraController::createInstance(iniFilePath);

	currentBufferSize = cameraBufferSize;
	inputLatency = currentBufferSize;		// typically
	outputLatency = currentBufferSize * 2;
	// typically blockFrames * 2; try to get 1 by offering direct buffer
	// access, and using asioPostOutput for lower latency

	samplePosition = 0;
	sampleRate = 44100.;
	milliSeconds = (long)((double)(cameraBufferSize * 1000) / sampleRate);
	active = false;
	started = false;
	timeInfoMode = false;
	tcRead = false;
	for (long i = 0; i < maxNumInputs; i++)
	{
		inputBuffers[i] = nullptr;
		inMap[i] = 0;
	}
	for (long i = 0; i < numOutputs; i++)
	{
		outputBuffers[i] = nullptr;
		outMap[i] = 0;
	}
	dataBuffer = nullptr;
	callbacks = 0;
	activeInputs = activeOutputs = 0;
	toggle = 0;
	delayFrames = 2;
}

//------------------------------------------------------------------------------------------
VSAsioDriver::~VSAsioDriver()
{
	stop();
	outputClose();
	inputClose();
	disposeBuffers();
}

//------------------------------------------------------------------------------------------
void VSAsioDriver::getDriverName(char *name)
{
	strcpy(name, "VS ASIO Driver");
}

//------------------------------------------------------------------------------------------
long VSAsioDriver::getDriverVersion()
{
	return 0x00000001L;
}

//------------------------------------------------------------------------------------------
void VSAsioDriver::getErrorMessage(char *string)
{
	strcpy(string, errorMessage);
}

//------------------------------------------------------------------------------------------
ASIOBool VSAsioDriver::init(void* sysRef)
{
	sysRef = sysRef;
	if (active)
		return true;
	strcpy(errorMessage, "ASIO Driver open Failure!");

	if (inputOpen())
	{
		if (outputOpen())
		{
			active = true;
			return true;
		}
	}
	timerOff();		// de-activate 'hardware'

	outputClose();
	inputClose();
	return false;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::start()
{
	if (callbacks)
	{
		started = false;
		samplePosition = 0;
		theSystemTime.lo = theSystemTime.hi = 0;
		toggle = 0;

		CameraController::getInstance().start();
		Sleep(2000);

		timerOn();			// activate 'hardware'
		started = true;

		return ASE_OK;
	}
	return ASE_NotPresent;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::stop()
{
	started = false;
	timerOff();		// de-activate 'hardware'

	CameraController::getInstance().stop();

	return ASE_OK;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::getChannels (long *outInputChannels, long *outOutputChannels)
{
	*outInputChannels = (long)CameraController::getInstance().getNumInputChannels();
	*outOutputChannels = numOutputs;
	return ASE_OK;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::getLatencies (long *_inputLatency, long *_outputLatency)
{
	*_inputLatency = inputLatency;
	*_outputLatency = outputLatency;
	return ASE_OK;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::getBufferSize (long *minSize, long *maxSize,
	long *preferredSize, long *granularity)
{
	*minSize = *maxSize = *preferredSize = cameraBufferSize;		// allow this size only
	*granularity = 0;
	return ASE_OK;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::canSampleRate (ASIOSampleRate sampleRate)
{
	if (sampleRate == CameraController::getInstance().getSampleRate())		// allow these rates only
		return ASE_OK;
	return ASE_NoClock;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::getSampleRate (ASIOSampleRate *sampleRate)
{
	*sampleRate = CameraController::getInstance().getSampleRate();
	return ASE_OK;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::setSampleRate (ASIOSampleRate sampleRate)
{
	if (sampleRate != CameraController::getInstance().getSampleRate())
		return ASE_NoClock;

	if (sampleRate != this->sampleRate)
	{
		this->sampleRate = sampleRate;
		asioTime.timeInfo.sampleRate = sampleRate;
		asioTime.timeInfo.flags |= kSampleRateChanged;
		milliSeconds = (long)((double)(cameraBufferSize * 1000) / this->sampleRate);
		if (callbacks && callbacks->sampleRateDidChange)
			callbacks->sampleRateDidChange (this->sampleRate);
	}
	return ASE_OK;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::getClockSources (ASIOClockSource *clocks, long *numSources)
{
	// internal
	clocks->index = 0;
	clocks->associatedChannel = -1;
	clocks->associatedGroup = -1;
	clocks->isCurrentSource = ASIOTrue;
	strcpy(clocks->name, "Internal");
	*numSources = 1;
	return ASE_OK;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::setClockSource (long index)
{
	if (!index)
	{
		asioTime.timeInfo.flags |= kClockSourceChanged;
		return ASE_OK;
	}
	return ASE_NotPresent;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::getSamplePosition (ASIOSamples *sPos, ASIOTimeStamp *tStamp)
{
	tStamp->lo = theSystemTime.lo;
	tStamp->hi = theSystemTime.hi;
	if (samplePosition >= twoRaisedTo32)
	{
		sPos->hi = (unsigned long)(samplePosition * twoRaisedTo32Reciprocal);
		sPos->lo = (unsigned long)(samplePosition - (sPos->hi * twoRaisedTo32));
	}
	else
	{
		sPos->hi = 0;
		sPos->lo = (unsigned long)samplePosition;
	}
	return ASE_OK;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::getChannelInfo (ASIOChannelInfo *info)
{
	int numInputChannels = CameraController::getInstance().getNumInputChannels();
	if (info->channel < 0 || (info->isInput ? info->channel >= numInputChannels : info->channel >= numOutputs))
		return ASE_InvalidParameter;
#if WINDOWS
	info->type = ASIOSTInt32LSB;
#else
	info->type = ASIOSTInt32LSB;
#endif
	info->channelGroup = 0;
	info->isActive = ASIOFalse;
	long i;
	if (info->isInput)
	{
		for (i = 0; i < activeInputs; i++)
		{
			if (inMap[i] == info->channel)
			{
				info->isActive = ASIOTrue;
				break;
			}
		}
		sprintf(info->name, "Microphone %d", info->channel);
	}
	else
	{
		for (i = 0; i < activeOutputs; i++)
		{
			if (outMap[i] == info->channel)
			{
				info->isActive = ASIOTrue;
				break;
			}
		}
		sprintf(info->name, "Output %d", info->channel);
	}
	
	
	return ASE_OK;
}

//------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::createBuffers (ASIOBufferInfo *bufferInfos, long numChannels,
	long bufferSize, ASIOCallbacks *callbacks)
{
	ASIOBufferInfo *info = bufferInfos;
	long i;
	bool notEnoughMem = false;
	int numInputChannels = CameraController::getInstance().getNumInputChannels();

	activeInputs = 0;
	activeOutputs = 0;
	currentBufferSize = bufferSize;
	for (i = 0; i < numChannels; i++, info++)
	{
		if (info->isInput)
		{
			if (info->channelNum < 0 || info->channelNum >= numInputChannels)
				goto error;
			inMap[activeInputs] = info->channelNum;
			inputBuffers[activeInputs] = new int[currentBufferSize * 2];	// double buffer
			if (inputBuffers[activeInputs])
			{
				info->buffers[0] = inputBuffers[activeInputs];
				info->buffers[1] = inputBuffers[activeInputs] + currentBufferSize;
			}
			else
			{
				info->buffers[0] = info->buffers[1] = 0;
				notEnoughMem = true;
			}
			activeInputs++;
			if (activeInputs > numInputChannels)
			{
error:
				disposeBuffers();
				return ASE_InvalidParameter;
			}
		}
		else	// output			
		{
			if (info->channelNum < 0 || info->channelNum >= numOutputs)
				goto error;
			outMap[activeOutputs] = info->channelNum;
			outputBuffers[activeOutputs] = new int[currentBufferSize * 2];	// double buffer
			if (outputBuffers[activeOutputs])
			{
				info->buffers[0] = outputBuffers[activeOutputs];
				info->buffers[1] = outputBuffers[activeOutputs] + currentBufferSize;
			}
			else
			{
				info->buffers[0] = info->buffers[1] = 0;
				notEnoughMem = true;
			}
			activeOutputs++;
			if (activeOutputs > numOutputs)
			{
				activeOutputs--;
				disposeBuffers();
				return ASE_InvalidParameter;
			}
		}
	}		
	if (notEnoughMem)
	{
		disposeBuffers();
		return ASE_NoMemory;
	}

	this->callbacks = callbacks;
	if (callbacks->asioMessage (kAsioSupportsTimeInfo, 0, 0, 0))
	{
		timeInfoMode = true;
		asioTime.timeInfo.speed = 1.;
		asioTime.timeInfo.systemTime.hi = asioTime.timeInfo.systemTime.lo = 0;
		asioTime.timeInfo.samplePosition.hi = asioTime.timeInfo.samplePosition.lo = 0;
		asioTime.timeInfo.sampleRate = sampleRate;
		asioTime.timeInfo.flags = kSystemTimeValid | kSamplePositionValid | kSampleRateValid;

		asioTime.timeCode.speed = 1.;
		asioTime.timeCode.timeCodeSamples.lo = asioTime.timeCode.timeCodeSamples.hi = 0;
		asioTime.timeCode.flags = kTcValid | kTcRunning ;
	}
	else
		timeInfoMode = false;	
	return ASE_OK;
}

//---------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::disposeBuffers()
{
	long i;
	
	callbacks = 0;
	stop();
	for (i = 0; i < activeInputs; i++)
		delete inputBuffers[i];
	activeInputs = 0;
	for (i = 0; i < activeOutputs; i++)
		delete outputBuffers[i];
	activeOutputs = 0;
	return ASE_OK;
}

//---------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::controlPanel()
{
	return ASE_NotPresent;
}

//---------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::future (long selector, void* opt)	// !!! check properties 
{
	ASIOTransportParameters* tp = (ASIOTransportParameters*)opt;
	switch (selector)
	{
		case kAsioEnableTimeCodeRead:	tcRead = true;	return ASE_SUCCESS;
		case kAsioDisableTimeCodeRead:	tcRead = false;	return ASE_SUCCESS;
		case kAsioSetInputMonitor:		return ASE_SUCCESS;	// for testing!!!
		case kAsioCanInputMonitor:		return ASE_SUCCESS;	// for testing!!!
		case kAsioCanTimeInfo:			return ASE_SUCCESS;
		case kAsioCanTimeCode:			return ASE_SUCCESS;
	}
	return ASE_NotPresent;
}

//--------------------------------------------------------------------------------------------------------
// private methods
//--------------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------------
// input
//--------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------
bool VSAsioDriver::inputOpen ()
{
	dataBuffer = new int[cameraBufferSize * maxNumInputs];

	return true;
}


//---------------------------------------------------------------------------------------------
void VSAsioDriver::inputClose ()
{
	if (dataBuffer)
		delete dataBuffer;
	dataBuffer = nullptr;
}

//---------------------------------------------------------------------------------------------
void VSAsioDriver::input()
{
	using namespace std::chrono;
	static milliseconds lastms;
	static std::ostringstream logStream;
	milliseconds ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
	auto difference = ms - lastms;
	long timeInMs = difference.count();
	double timeInSeconds = (double)timeInMs / 1000.0;
	//logStream << "Took " << timeInMs << " milliseconds\n";
	//std::string log = logStream.str();
	lastms = ms;
	auto& channelMap = CameraController::getInstance().getChannelMap();

	for (long channel = 0; channel < activeInputs; channel++) {
		int* in = inputBuffers[channel];
		if (in) {
			if (toggle) {
				in += currentBufferSize;
			}

			int cameraChannel = channelMap[channel];
			memcpy(in, dataBuffer + cameraChannel * cameraBufferSize, sizeof(int) * currentBufferSize);
		}
	}

	if (delayFrames > 0)
	{
		delayFrames--;
	}
	else
	{
		//Delay reading from camera by one buffer so it has time to actually acquire data
		int result = CameraController::getInstance().getDataBlock(dataBuffer);
		int numIterations = 0;
		while (result == -16399) {
			result = CameraController::getInstance().getDataBlock(dataBuffer);
			Sleep(1);
			numIterations++;
		}
		if (numIterations > 0) {
			int breakHere = 0;
		}
		if (result)
		{
			delayFrames = 2;
			memset(dataBuffer, 0, sizeof(int)*cameraBufferSize*maxNumInputs);
		}
	}
}

//------------------------------------------------------------------------------------------------------------------
// output
//------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------
bool VSAsioDriver::outputOpen()
{
	return true;
}

//---------------------------------------------------------------------------------------------
void VSAsioDriver::outputClose ()
{
}

//---------------------------------------------------------------------------------------------
void VSAsioDriver::output ()
{
}

//---------------------------------------------------------------------------------------------
void VSAsioDriver::bufferSwitch ()
{
	if (started && callbacks)
	{
		getNanoSeconds(&theSystemTime);			// latch system time
		input();
		output();
		samplePosition += currentBufferSize;
		if (timeInfoMode)
			bufferSwitchX ();
		else
			callbacks->bufferSwitch (toggle, ASIOFalse);
		toggle = toggle ? 0 : 1;
	}
}

//---------------------------------------------------------------------------------------------
// asio2 buffer switch
void VSAsioDriver::bufferSwitchX ()
{
	getSamplePosition (&asioTime.timeInfo.samplePosition, &asioTime.timeInfo.systemTime);
	long offset = toggle ? currentBufferSize : 0;
	if (tcRead)
	{	// Create a fake time code, which is 10 minutes ahead of the card's sample position
		// Please note that for simplicity here time code will wrap after 32 bit are reached
		asioTime.timeCode.timeCodeSamples.lo = asioTime.timeInfo.samplePosition.lo + 600.0 * sampleRate;
		asioTime.timeCode.timeCodeSamples.hi = 0;
	}
	callbacks->bufferSwitchTimeInfo (&asioTime, toggle, ASIOFalse);
	asioTime.timeInfo.flags &= ~(kSampleRateChanged | kClockSourceChanged);
}

//---------------------------------------------------------------------------------------------
ASIOError VSAsioDriver::outputReady ()
{
	return ASE_NotPresent;
}

//---------------------------------------------------------------------------------------------
double AsioSamples2double (ASIOSamples* samples)
{
	double a = (double)(samples->lo);
	if (samples->hi)
		a += (double)(samples->hi) * twoRaisedTo32;
	return a;
}

