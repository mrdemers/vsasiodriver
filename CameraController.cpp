#include "CameraController.h"

#include "INIReader.h"

#ifdef __cplusplus
extern "C" {
#endif
#include "aif-dll.h"
#ifdef __cplusplus
}
#endif

std::unique_ptr<CameraController> CameraController::cameraController;

void CameraController::createInstance(std::string iniFilePath)
{
	if (cameraController == nullptr)
	{
		cameraController = std::make_unique<CameraController>(iniFilePath);
	}
}

CameraController& CameraController::getInstance()
{
	return *cameraController.get();
}

CameraController::CameraController(std::string iniFilePath)
{
	INIReader iniReader(iniFilePath);
	int numInputChannels = (int)iniReader.GetInteger("settings", "microphone_count", 64);
	int gainSetting = (int)iniReader.GetInteger("settings", "gain", GainSetting::Medium);
	sampleRate = (double)iniReader.GetReal("settings", "sample_rate", 48000.);
	int latencyBuffers = (int)iniReader.GetInteger("settings", "latency_buffers", 200);

	if (numInputChannels == 86) {
		//Germany Sphere channel mapping
		cameraChannelMap = {
			0,   1,  2,  3,  4,  5,
			16, 17, 18, 19, 20,
			32, 33, 34, 35, 36, 37,
			48, 49, 50, 51, 52,

			64, 65, 66, 67, 68,
			80, 81, 82, 83, 84, 85,
			96, 97, 98, 99, 100,
			112, 113, 114, 115, 116, 117,

			128, 129, 130, 131, 132,
			144, 145, 146, 147, 148,
			160, 161, 162, 163, 164, 165,
			176, 177, 178, 179, 180, 181,

			192, 193, 194, 195, 196,
			208, 209, 210, 211, 212,
			224, 225, 226, 227, 228,
			240, 241, 242, 243, 244
		};
	}
	else {
		//Generic channel map, use all channels
		for (int channel = 0; channel < numInputChannels; channel++) {
			cameraChannelMap.push_back(channel);
		}
	}

	std::string bitFile;
	if (numInputChannels > 64)
	{
		if (sampleRate == 44100.)
		{
			switch (gainSetting)
			{
			case Low: bitFile = "starlink_v431.bit"; break;
			case Medium: bitFile = "starlink_v434.bit"; break;
			case High: bitFile = "starlink_v437.bit"; break;
			default:
				bitFile = "starlink_v434.bit";
			}
		}
		else if (sampleRate == 48000.)
		{
			switch (gainSetting)
			{
			case Low: bitFile = "starlink_v421.bit"; break;
			case Medium: bitFile = "starlink_v424.bit"; break;
			case High: bitFile = "starlink_v427.bit"; break;
			default:
				bitFile = "starlink_v424.bit";
			}
		}
	}
	else
	{
		switch (gainSetting)
		{
		case Low: bitFile = "starlink_v205.bit"; break;
		case Medium: bitFile = "starlink_v206.bit"; break;
		case High: bitFile = "starlink_v207.bit"; break;
		default:
			bitFile = "starlink_v206.bit";
		}
	}

	auto lastSlashPosition = iniFilePath.rfind("\\");
	auto dllFolder = iniFilePath.substr(0, lastSlashPosition + 1);
	auto bitFilePath = dllFolder + bitFile;

	_vs_aif_init();

	int fpgaChannels = numInputChannels > 64 ? 256 : 64;
	int internalBufferSize = cameraBufferSize * fpgaChannels * sizeof(int) * latencyBuffers;


	int openResult = _vs_aif_acquisition_open(
		&aifHandle,
		0,
		fpgaChannels,
		cameraBufferSize,
		internalBufferSize,
		(char*)bitFilePath.c_str(),
		0);
}

CameraController::~CameraController()
{
	stop();
	_vs_aif_acquisition_close(aifHandle);
}

void CameraController::start()
{
	_vs_aif_acquisition_start(aifHandle);
}

void CameraController::stop()
{
	_vs_aif_acquisition_stop(aifHandle);
}

double CameraController::getSampleRate()
{
	return sampleRate;
}

std::vector<int>& CameraController::getChannelMap()
{
	return cameraChannelMap;
}

int CameraController::getNumInputChannels()
{
	return cameraChannelMap.size();
}

int CameraController::getDataBlock(int* dataBuffer)
{
	return _vs_aif_retrieve_data_block(aifHandle, dataBuffer);
}