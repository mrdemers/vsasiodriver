Installation:

Right click the install.bat file, and select Run as administrator. This will register the vsAsioDriver.dll as an ASIO driver in windows

Now in a DAW you should see the audio device Visisonics ASIO Driver under the asio options.

If you do not see the driver show up, first verify that the driver was registered. In the windows start bar, type regedit and run the program.
Expand the dropdown menus HKEY_LOCAL_MACHINE->SOFTWARE->ASIO. There should be an entry labeled VS ASIO Driver. If there is no entry, try rebooting the machine and check again. If there are still issues, contact Visisonics for support



How to use:

The asio driver is controlled through the vsAsioDriver.ini text file. There are four settings that can be controlled:
  1. sample_rate - Can either be 44100 or 48000. Anything else will default to 48000
  2. gain - Can be 0, 1, or 2. These will change the bit file used to 421, 424, or 427 with 48kHz, or 431, 434, or 437 with 44.1kHz sample rate
  3. microphone_count - leave as 86 to interface with the custom sphere
  4. latency_buffers - Determines the number of buffers to allocate for sound acquisition. A lower number will result in more audio pops, but a higher number will introduce more sample latency in recordings

You must restart any DAW or program using the ASIO driver after changing these parameters to apply them

When using the asio driver in a DAW, no sound will be output to the speakers or headphones. It will simply allow you to record the audio coming from the camera. To hear the audio, you will have to a different audio device after you have recorded some data

To use the ASIO driver in Reaper, you must enable the inputs microphone 0 - microphone 85
Then to record all 86 microphones you must create 11 tracks. 
On each track click the "Route" button and change the track channels to 8, except change the last one to 6. 
Change the input channels for track 1 to Microphone 0 - Microphone 7, track 2 to Microphone 8 - Microphone 15, etc...
Prime each track for recording by clicking the record button on the track, and then click the master record button.
Render the recording to a file. First change the Source at the top to Stems (selected tracks), and then select all 11 tracks
Change the sample rate to match the project, and enable the checkbox Multichannel tracks to multichannel files.
Render the 11 files, and then you can use a program such as audacity to stitch the 11 files into one 86 channel wav file
